package com.winyluter.security.demo.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Create by winyluter
 * @date 2018/12/26 10:17
 */
@Target({ElementType.METHOD, ElementType.FIELD})//可以比较在什么地方
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MyContraintValidator.class)
public @interface MyConstraint {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
