package com.winyluter.security.demo.exception;

/**
 * @author create by winyluter
 * @date 2018/12/27 0:21
 */
public class UserNotExistException extends RuntimeException {

    private static final long serialVersionUID = -1L;

    private String id;

    public UserNotExistException(String id) {
        super("user not exist");
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
