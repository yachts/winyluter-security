package com.winyluter.security.demo.validator;

import com.winyluter.security.demo.service.HelloServer;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Create by winyluter
 * @date 2018/12/26 10:20
 */
public class MyContraintValidator implements ConstraintValidator<MyConstraint, Object> {


    @Autowired
    private HelloServer helloServer;

    @Override
    public void initialize(MyConstraint myConstraint) {
        System.out.println("my validator init");
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        helloServer.greeting("tom");
        System.out.println(value);
        return false;
    }
}
