package com.winyluter.security.demo.service;

/**
 * @author Create by winyluter
 * @date 2018/12/26 10:32
 */
public interface HelloServer {

    String greeting(String name);
}
