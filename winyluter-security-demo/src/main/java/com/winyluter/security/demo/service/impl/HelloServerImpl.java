package com.winyluter.security.demo.service.impl;

import com.winyluter.security.demo.service.HelloServer;
import org.springframework.stereotype.Service;

/**
 * @author Create by winyluter
 * @date 2018/12/26 10:33
 */
@Service
public class HelloServerImpl implements HelloServer {

    @Override
    public String greeting(String name) {
        System.out.println("greeting");
        return "hello" + name;
    }
}
